use aws_config::load_from_env;
use aws_sdk_rds::Client;
use clap::Parser;
use db_base::{
    api_run, objects::structures::Args
};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {

    let args = Args::parse();

    let config = load_from_env().await;
    println!("Config - {}", config.region().unwrap());
    let client_rds = Client::new(&config);

    match api_run(args, client_rds).await {
        Ok(ret) => println!("Success - {}", ret),
        Err(e) => println!("Error - {}", e)
    };

    Ok(())
}

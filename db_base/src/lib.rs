pub mod objects;
pub mod db;

use objects::structures::Args;
use db::client_db::*;
use aws_sdk_rds::Client;
use sqlx::postgres::PgConnection;

pub async fn api_run(args: Args, client: Client) -> Result<String, Box<dyn std::error::Error + Send + Sync>> {
    let _res: PgConnection = get_client(args, client).await?;
    Ok("Success".to_string())
}
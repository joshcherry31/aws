use std::collections::HashMap;

use crate::objects;

use objects::structures::*;
use sqlx::{postgres::PgConnection, Connection, mysql::MySqlConnection};
use aws_sdk_rds::Client;


pub async fn get_client<T>(
    args: Args, 
    client: Client
) -> Result<T, Box <dyn std::error::Error + Send + Sync>>
where
    T: Connection
{
    println!("Here - {}", &args.db_name);
    let response: aws_sdk_rds::operation::describe_db_instances::DescribeDbInstancesOutput = match client
        .describe_db_instances()
        .db_instance_identifier("testdb")
        .send()
        .await {
            Ok(res) => res,
            Err(e) => {
                println!("Err - {:#?}", e);
                return Err(Box::new(e));
            }
        };
        println!("Here");
    let instances = match response.db_instances {
        Some(instances) => instances,
        None => return Err("Found not db instance".into()),
    };
    let instance = instances.get(0).unwrap();
    println!("db_name - {}", instance.db_name.clone().unwrap());
    let mut conn_list = HashMap::new();
    let tag = {
        let tag_list = instance.tag_list.clone().unwrap();
        let mut val = String::new();
        for tag in tag_list {
            if tag.key.unwrap() == "db_type".to_string() {
                val = tag.value.unwrap();
            }
        }
        val
    };
    println!("Db type - {}", tag);
    let endpoint = instance.endpoint.clone().unwrap();
    let connection_string = format!("{}://{}:{}@{}:{}", tag, args.username, args.pw, endpoint.address.unwrap(), endpoint.port.unwrap());
    println!("{} {} {}", instance.db_instance_identifier.clone().unwrap(), &args.db_name, &connection_string);
    conn_list.insert(instance.db_instance_identifier.clone().unwrap(), &connection_string);
    let conn_str = match conn_list.get(&args.db_name) {
        Some(conn_str) => conn_str,
        None => &"None".to_string()
    };
    println!("Connection string - {}", conn_str);
    let conn = match T::connect(&connection_string).await {
        Ok(res) => {
            println!("HERE");
            res
        },
        Err(e) => {
            println!("Failure");
            return Err(Box::new(e));
        }
    };
    
    Ok(conn)
}

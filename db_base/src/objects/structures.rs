#![allow(dead_code)]
use clap::Parser;

#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about= None)]
pub struct Args {
    #[arg(short, long)]
    pub db_name: String,
    
    #[arg(short, long)]
    pub username: String,
    
    #[arg(short, long)]
    pub pw: String,

}

#[derive(Parser, Debug, Clone)]
pub struct RdsClient {
    db_name: String,
}
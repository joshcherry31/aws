Instructions for creating the httpd access cloudwatch agent
    1.  sudo yum install -y amazon-cloudwatch-agent
    2.  Create the config file with this command and copy the contents of cloudwatch_agent_config.json here
            sudo vim /tmp/cloudwatch-agent-config.json 
    3.  There is no need to change the instance id in the json file as the cloudwatch agent will infer that value as the instance id of the current instance running.
    4.  Run this command
            sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -c file:/tmp/cloudwatch-agent-config.json -s
    5.  Go to log groups to find your new log group, select metric and slect create metric filter.
    6.  To find 404 status codes or any status codes you would like to identify by entering this regex into the Filter pattern %\b404\b%. You can exchange the 404 with whatever other pattern you'd like to identify.
            The %\b is required at the begining and end of any regex expresion when setting up a filter pattern. 
    7.  Make sure to set up your filter name, metirc namespace, name, value and unit appropriately.
    8.  Once that is done you go to 'All metrics', select Graphed metrics and you'll find your metric there.
    9.  Select the bell icon to set up a cloudwatch alarm.
    10. Leave most of the values as default but change threshold value to 0. Click next
    11. Now we will create an sns topic by selecting 'In Alarm', 'Create new topic' make sure the topic name is unique and enter the email you would like to recieve the alarms to.
    12. Go to your email and confirm the subscription to the topic.
    13. Give your alarm a name, hit create and that is it.

CORS policy instructions
        Under the allowed origins we will want to replace the "*" with the url of the bucket that is accessing the objects in the bucket that we are trying to allow cross origin resource sharing for.

In the setup sql for athena, at the botttom you should replace the s3 bucket with the bucket in which cloudtrail is set up to use. I removed my account id from the code for security reasons
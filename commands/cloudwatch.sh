#!/bin/bash
# This command installs cloud watch agent needed for advanced ec2 metrics

install_cmmd () {
    dnf install amazon-cloudwatch-agent -y
}


# This will configure the cw agent. We do not want to monitor for collectd if we do not have it installed.
# Agregate invterval was set to 10 on install. This is monitoring rsyslog so /var/log/messages is the log path.
# config file for cw agent is in /opt/aws/amazon-cloudwatch-agent/bin
configure_cw () {
    /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-config-wizard
}

# Starts the cloud watch agent
start_agent() {
    /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/opt/aws/amazon-cloudwatch-agent/bin/config.json
}
#!/bin/bash
###########################################################################################################################
## This is just a file I am creating as a refernece so I can remember and study the commands I have used in the past.
## Eventually I might make a script out of it to do something. Need an idea first
###########################################################################################################################

kms_ecrypt() {
    aws kms encrypt --key-id $1 --plaintext fileb://$2 --output text --query CiphertextBlob | base64 --decode > $3
}

kms_decrypt() {
    aws kms decrypt --ciphertext-blob fileb://$1 --output text --query Plaintext | base64 --decode > $2
}

kms_re_ecrypt() {
    aws kms re-encrypt --destination-key-id $1 --ciphertext-blob fileb://$2 | base64 > $3
}

kms_enable_rotation() {
    aws kms enable-key-rotation --key-id  $1
}

kms_rotation_status() {
    aws kms get-key-rotation-status --key-id  $1
}

kms_generate_data_key{
    aws kms generate-data-key --key-id  $1 --key-spec AES_256
}
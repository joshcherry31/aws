#!/bin/bash
###########################################################################################################################
## This is just a file I am creating as a refernece so I can remember and study the commands I have used in the past.
## Eventually I might make a script out of it to do something. Need an idea first
###########################################################################################################################

# List buckets
list_s3_buckets() {
    aws s3 ls
}

# List buckets
list_specific_buckets() {
    aws s3 ls s3://$1
}

# Make bucket
make_bucket() {
    aws s3 mb s3://$1
}

# Remove a file
delete_s3_bucket () {
    aws s3 rb s3://$1
}

# Upload a file
upload_s3 () {
    aws s3 cp $1 s3://$2
}

# Remove a file
remove_s3_file () {
    aws s3 rm s3://$1/$2
}

# List bucket contents while setting the page size. This one ends up making api calls to get 100 items until it gets all the items
# We set the page size to avoid time out errors
list_bucket_objects() {
    aws s3api list-objects --bucket $1 --page-size 100
}

# list bucket contents while setting the page size. This one will only get the specified number of items
list_bucket_objects() {
    aws s3api list-objects --bucket $1 --max-items $2
}
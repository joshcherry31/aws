#!/bin/bash
###########################################################################################################################
## This is just a file I am creating as a refernece so I can remember and study the commands I have used in the past.
## Eventually I might make a script out of it to do something. Need an idea first
###########################################################################################################################

# Should probably keep the read and write capacity low like 5 or something
create_table() {     
    aws dynamodb create-table --table-name $1 --attribute-definitions AttributeName=fort_id, AttributeType=N --key-schema AttributeName=UserID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=$2, WriteCapacityUnits=$3
}

write_to_table() {
    aws dynamodb batch-write-item --request-items file://$1
}

# id should be in json format like '{"Id":{"N":"403"}}'
get_item() {
    aws dynamodb get-item --table-name $1 --region $2 --key $3
}

aws dynamodb get-item --table-name BillTable --region us-east-1 --key '{\"ID\":{\"N\":\"0\"}}'
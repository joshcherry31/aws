const express = require('express');
const cors = require('cors');
const app = express();
const fs = require('fs')

app.use(cors());
app.use(express.json());
 
const port = 3000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

app.post("/test", (request, response) => {
    const status = {
       "Status": "Success"
    };
    
    console.log("Received:", request.body);
    debug(request.body.FirstName);
    response.send(status);
});


function debug(text) {
  var dbg_text = text.concat("\n");
  var file_name = ""

  file_name = "/logs/website_log.txt";

  fs.appendFile(file_name, dbg_text, (err) => {
    if (err) throw err;
  });
}

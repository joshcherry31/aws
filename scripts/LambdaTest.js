const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const { DynamoDBDocument  } = require("@aws-sdk/lib-dynamodb");

const client = new DynamoDBClient({});
const docClient = DynamoDBDocument.from(client);

const TABLE_NAME = process.env.table_name;
const EMAIL_ADDRESS = process.env.email;

exports.handler = async (event) => {
    let { Type, Amount } = event;

    console.log(`Type: ${Type}`);
    console.log(`Amount: ${Amount}`);

  try {
    let unique_key = "1235";
    const item = {
        "BillKey": unique_key,
        Type: Amount,
    };
    
    await docClient.put({
      TableName:"BillTable",
      Item: item
    });
    
    const response = {
      statusCode: 200,
      body: JSON.stringify({
          message: "Success",
          receivedType: Type,
          receivedAmount: Amount
      }),
    };
    
    return response;
  } catch (error) {
      return {
          statusCode: 500,
          body: JSON.stringify({
              message: "Internal server error",
              error: error.message
          }),
      };
  }
};

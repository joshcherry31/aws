-- Create the database
CREATE DATABASE IF NOT EXISTS PrimaryDB;

-- Connect to the bills database and create the table
\c PrimaryDB

-- Create the bill_expenses table
CREATE TABLE bill_expenses (
    bill_name VARCHAR(50) PRIMARY KEY,
    january DECIMAL(10, 2),
    february DECIMAL(10, 2),
    march DECIMAL(10, 2),
    april DECIMAL(10, 2),
    may DECIMAL(10, 2),
    june DECIMAL(10, 2),
    july DECIMAL(10, 2),
    august DECIMAL(10, 2),
    september DECIMAL(10, 2),
    october DECIMAL(10, 2),
    november DECIMAL(10, 2),
    december DECIMAL(10, 2)
);

-- Insert rows into the bill_expenses table
INSERT INTO bill_expenses (bill_name) VALUES
('phone_bill'),
('Medical_dental'),
('TV_Internet'),
('CPS'),
('Jeep'),
('Prius'),
('Insurance'),
('MCC'),
('SCC'),
('STCC'),
('rent'),
('BBCC'),
('School'),
('Groceries'),
('Bally'),
('Hulu'),
('League_Pass'),
('Netflix'),
('Golds'),
('Student_Loans'),
('Paramount_plus'),
('Max'),
('acloudguru'),
('AWS'),
('gas'),
('FastFood_Restaraunts'),
('Amazon_primes');

-- Create the database
CREATE DATABASE IF NOT EXISTS PrimaryDB;

-- Connect to the bills database and create the table
\c PrimaryDB

-- Create the user table
CREATE TABLE user (
    email VARCHAR(200) UNIQUE KEY,
    First_Name VARCHAR(50),
    last_Name VARCHAR(50),
);

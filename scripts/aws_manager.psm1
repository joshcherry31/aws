#############################################################################################################################################################################################################################################################
#   Written by Joshua Cherry
#   Usage:  
#           Using module ./aws_manager.psm1 
#           [AWSManager]$var = [AWSManager]::new([string]PathToKeyLocation)
#
#           Run this after making changes
#           Remove-Module -Name aws_manager -Force
#
#   Notes:
#   In the future the credential setting will be removed as all of this will be moved to a ec2 instace with the role to execute all these functionalities.
#############################################################################################################################################################################################################################################################

class AWSManager {

    [string]$AccKey
    [string]$SecKey
    [object]$server

    AWSManager ([string]$CF) {	
        $content = Get-Content -path $CF

        $keys = $content[1].split(',')
        $this.AccKey = $keys[0]
        $this.SecKey = $keys[1]
        Set-AWSCredential -AccessKey $this.AccKey -SecretKey $this.SecKey
    }

    [void]ShowInstances() {
        $details = (Get-EC2Instance).Instances

        foreach ($instance in $details) {
            Write-Host $instance
            $inst_id = $instance.InstanceId
            Write-Host "Instance id - $inst_id"
        }
    }

    [void]ConnectToEC2([string]$Name, [string]$Region, [string]$CF) {
        Set-AWSCredential -AccessKey $this.AccKey -SecretKey $this.SecKey
        $this.server = New-Object -TypeName AWSServer -ArgumentList $Name, $Region, $CF
        Write-Host "Connected to " $this.server.Name
    }

    #########################################################################################################################################################################################################################################################
    #   [void]Deploy_VPC([string]$EC2_Name, [string]$Region, [string]$VPC_Name)
    #   This method will take an EC2 instance and deploy it to a vpc
    #########################################################################################################################################################################################################################################################
    [void]Deploy_VPC([string]$EC2_Name, [string]$Region, [string]$Subnet, [string]$CF) {
        Set-AWSCredential -AccessKey $this.AccKey -SecretKey $this.SecKey
        $this.ConnectToEC2($EC2_Name, $Region, $CF)
        Write-Host "Stopping " $this.server.Name
        $this.server.StopMachine()
        Write-Host "Creating snapshots for " $this.server.Name
        $this.server.CreateSS()
        Write-Host "Attempt to deploy to VPC "
        $this.server.RestoreSS($Subnet, $Region)
    }
} 

class AWSServer {
    

    [string]$AccessKey
    [string]$SecretKey
    [string]$Name                           ## Var for instance Name
    [string]$Region                         ## Var for the instance Region
    [string]$InstanceId                     ## The most important var, the instance ID
    [string]$Profile                        ## Which profile the instance belongs too, QA/DEV/PROD/BETA
    [string]$Application                    ## This is for the application tag, what is using this server
    [string]$SubnetId                       ## We need to know which subnet were in so we can move to other AZ if needed
    [string]$PrivateIP                      ## Making sure we end up with the same address for the previous server 
    [string]$PublicIP                       ## Making sure we end up with the same address for the previous server
    [string]$NetIntId                       ## 
    [string]$KeyName                        ## 
    [string]$NewKeyName                     ## 
    [string]$InstType                       ## 
    [string]$Platform                       ## 
    [string]$AmiID                          ## 
    [string]$ImageName                      ## 
    [string]$RootDevName                    ## We need this to know which snapshot to restore from
    [string]$RootDevType                    ## 
    [string]$BlockDevMap                    ## 
    [string]$SecGroup                       ## 
    [string]$IAMProfile                     ## 
    [string]$VPCID                          ## 
    [string]$StateStatus                    ## 
    [object]$NewServer                      ## To hold any new server created by the restore function

    #########################################################################################################################################################################################################################################################
    #   CONSTRUCTORS
    #   AWSServer()                     
    #               First constructor should rarely be used. I can't think of any time I would use it but its a standard constructor for 
    #               when we have zero info for the server
    #   
    #   AWSServer([string]$Name, [string]$Region, [string]PathToKeyLocation)
    #               Second constructor only needs the Name for the server and the Region it is located in. With this these parameters the 
    #               method is able to decipher the InstanceId, tags and other important information related to the instance.
    #   
    #   Usage: [AWSServer]$var = [AWSServer]::new()
    #          [AWSServer]$var = [AWSServer]::new([string]NAME_OF_SERVER,[string]REGION, [string]CONFIGURATION_FILE)
    #
    #   Notes:
    #           The access and secret keys are currently hard coded but this needs to be changed.
    #########################################################################################################################################################################################################################################################
    AWSServer(){
        $this.Name = "Undefined"
    }

    AWSServer([string]$Name, [string]$Region, [string]$CF){
        $content = Get-Content -path $CF

        $keys = $content[1].split(',')
        $this.AccessKey = $keys[0]
        $this.SecretKey = $keys[1]
        Set-AWSCredential -AccessKey $this.AccessKey -SecretKey $this.SecretKey

        ###################################################################################################
        ## This is where the objects variables are set. If new variables need to be set please add here. ##
        ## I choose not to set some variables because they require methods to be written that would      ##
        ## have no effect on my account                                                                  ##
        ###################################################################################################
        $this.Name = $Name
        $this.Region = $Region
        $this.InstanceId = Get-EC2Tag -Filter @{Name='tag:Name';Value=$Name} -Region $Region | Where-Object -Property ResourceType -eq "instance" | Select-Object -ExpandProperty ResourceId
        $details = (Get-EC2Instance -InstanceId $this.InstanceId -Region $this.Region).instances
        $details | ForEach-Object{
            $this.PrivateIP = $_.PrivateIpAddress
            $this.PublicIP = $_.PublicIpAddress
            $this.NetIntId = $_.NetworkInterfaces.NetworkInterfaceId
            $this.SubnetId = $_.SubnetId
            $this.KeyName = $_.KeyName
            $this.InstType = $_.InstanceType
            $this.Platform = $_.Platform
            $this.AmiID = $_.ImageID
            $this.ImageName = (Get-EC2Image -ImageId $_.ImageID -Region $this.Region).Name
            $this.RootDevName = $_.RootDeviceName
            $this.RootDevType = $_.RootDeviceType
            $this.BlockDevMap = $_.BlockDeviceMappings.DeviceName
            $this.SecGroup = $_.SecurityGroups
            $this.IAMProfile = $_.IamInstanceProfile
            $this.VPCID = $_.VpcId
            $this.StateStatus = $_.State.Name.Value
            # $this.Application = ((Get-EC2Instance -InstanceId $this.InstanceId -Region $this.Region).instances.Tags | Where-Object {$_.Key -eq "Application"}).Value
        }
    }

    #########################################################################################################################################################################################################################################################
    #   [void]StartMachine()
    #   Method only starts the machine. Must have initialized the Instance ID and Region before calling this method
    #########################################################################################################################################################################################################################################################
    [void]StartMachine(){        
        Set-AWSCredential -AccessKey $this.AccessKey -SecretKey $this.SecretKey
        Start-EC2Instance -InstanceID $this.InstanceID -Region $this.Region
        $this.WaitNewSS()
    }

    #########################################################################################################################################################################################################################################################
    #   [void]StopMachine()
    #   Method only stops the machine. Must have initialized the Instance ID and Region before calling this method
    #########################################################################################################################################################################################################################################################
    [void]StopMachine(){
        Set-AWSCredential -AccessKey $this.AccessKey -SecretKey $this.SecretKey
        Stop-EC2Instance -InstanceID $this.InstanceID -Region $this.Region
        while((Get-EC2Instance -InstanceID $this.InstanceId -Region $this.Region).instances.State.Name.Value -ne "stopped"){
            start-sleep -Seconds 10
            Write-Host "Waiting for instance to stop"
        }
    }

    #########################################################################################################################################################################################################################################################
    #   [void]RebootMachine()
    #   Method only stops the machine. Must have initialized the Instance ID and Region before calling this method
    #########################################################################################################################################################################################################################################################
    [void]RebootMachine(){
        Set-AWSCredential -AccessKey $this.AccessKey -SecretKey $this.SecretKey
        Restart-EC2Instance -InstanceID $this.InstanceID -Region $this.Region
        $this.WaitNewSS()
    }

    #########################################################################################################################################################################################################################################################
    #   [void]WaitNewSS()
    #   Waits for a machine to be in the running state
    #########################################################################################################################################################################################################################################################
    [void]WaitNewSS(){        
        while((Get-EC2Instance -InstanceID $this.NewServer.instances.InstanceId -Region $this.Region).instances.State.Name.Value -ne "running"){
            start-sleep -Seconds 10
            Write-Host "Waiting for instance to run"
        }
    }

    #########################################################################################################################################################################################################################################################
    #   METHODS
    #   [void] CreateSS()
    #   The variables of the Object including the Instance Id must be set before calling this function. The machine takes time to stop so I don't do 
    #   it in this method so you may want to call the stop and start methods before calling this function acompanied with a sleep call with enough time. 
    #########################################################################################################################################################################################################################################################
    [Object] CreateSS(){
                
        Set-AWSCredential -AccessKey $this.AccessKey -SecretKey $this.SecretKey
        $SSList = @()
        if($Null -ne $this.InstanceID){
            
            $volumes = Get-EC2Volume -Region $this.Region | Where-Object { $_.Attachments.InstanceId -eq $this.InstanceId }
            
            foreach($volume in $volumes){
                
                $NameTag = @{Key="Name";Value=$this.Name}
                $NameTagSpec = New-Object Amazon.EC2.Model.tagSpecification
                $NameTagSpec.ResourceType = "snapshot"
                $NameTagSpec.Tags.add($NameTag) 

                $CCTag = @{Key="CostCenter";Value=$this.CostCenter}
                $CCTagSpec = New-Object Amazon.EC2.Model.tagSpecification
                $CCTagSpec.ResourceType = "snapshot"
                $CCTagSpec.Tags.add($CCTag)

                $SnapshotId = New-EC2SnapShot -VolumeID $volume.VolumeID -TagSpecification $NameTagSpec -Description "Snapshot $NameTag" -Region $this.Region
                # New-EC2Tag -ResourceId  $SnapshotId.snapshotId -Tag @{Key="Application";Value=$this.Application} -Region $this.Region
                $SSList += $SnapshotId
            }
        }
        while((Get-EC2SnapShot -Region $this.Region -Owner self | Where-Object { $_.VolumeId -eq ($this.GetRootVolId()).VolumeId}).State -ne "completed"){
            Write-Host "Waiting for Snapshot to complete"
            start-sleep -Seconds 10
        }

        return $SSList
    }

    #########################################################################################################################################################################################################################################################
    #   [void]GetSS()
    #   This method returns all the snapshots related to the current server
    #########################################################################################################################################################################################################################################################
    [Object]GetSS(){
        $volumes = Get-EC2Volume -Region $this.Region | Where-Object { $_.Attachments.InstanceId -eq $this.InstanceId } 

        $Snapshots = Get-EC2SnapShot -Region $this.Region -Owner self
        $Snapshot = @()
        foreach($s in $Snapshots){
            foreach($v in $volumes){
                if($v.VolumeId -eq $s.VolumeId){
                    $Snapshot += $s
                }
            }
        }
        return $Snapshot
    }      
        
    #########################################################################################################################################################################################################################################################
    #   [void]GetRootVolId()
    #   Searches through each volume related to the current server and returns the root volume   
    #########################################################################################################################################################################################################################################################
    [Object]GetRootVolId(){
        
        $RootVolume = "" ## Placeholder for the RootVolume variable. This needs to be instantiated here because of scoping problems
        
        ########################################################################################################
        ## The eureka moment I had when I figured out how to find the root volume ID was that the attachments ##
        ## Contains an instance object and not the isntance ID like I had previously beleived. To access the  ##
        ## root device name you must go into the attachments and select the device like in line 195.          ##
        ########################################################################################################
        
        $volumes = Get-EC2Volume -Region $this.Region | Where-Object { $_.Attachments.InstanceId -eq $this.InstanceId }        
        
        foreach($v in $volumes){
            if($v.attachments.device -eq $this.RootDevName){
                $RootVolume = $v
            }
        }
        return $RootVolume
        
    }

    #########################################################################################################################################################################################################################################################
    #   [Object]GetVolumes()
    #   Returns all volumes related to the current server
    #########################################################################################################################################################################################################################################################
    [Object]GetVolumes(){
        $volumes = Get-EC2Volume -Region $this.Region | Where-Object { $_.Attachments.InstanceId -eq $this.InstanceId } 
        return $volumes
    }
    #########################################################################################################################################################################################################################################################
    #   [Object]GetEbs()
    #   Returns the block device mapping. Is returned in order
    #########################################################################################################################################################################################################################################################
    [Object]GetEbs(){
        return (Get-EC2Instance -InstanceId $this.InstanceId -Region $this.Region).instances.BlockDeviceMappings         
    }

    #########################################################################################################################################################################################################################################################
    #   [void]SetTags()
    #   This method sets the tags for the new server created in the restore function. Does no work with blank servers
    #########################################################################################################################################################################################################################################################
    [void]SetTags(){        
        
        $NewName = $this.Name + "Test"       ## To rename the machine
        $NewAMI = $this.Name + "_AMI"        ## To rename the AMI   

        New-EC2Tag -ResourceId  $this.NewServer.Instances.InstanceId -Tag @{Key="Name";Value=$NewName} -Region $this.Region
        New-EC2Tag -ResourceId  $this.NewServer.Instances.InstanceId -Tag @{Key="AMI";Value=$NewAMI} -Region $this.Region
    }
    
    #########################################################################################################################################################################################################################################################
    #   [void]RestoreSS()
    #   This Method first finds the volumes related to this server then finds the snapshots related to those volumes. Once we know the number of snapshots,
    #   we enter a switch statement that will create an AMI with the appropriate amount of additional storage volumes. The switch statement will accommodate
    #   up to five additional storage volumSes. This can change if needed.
    #########################################################################################################################################################################################################################################################
    [void]RestoreSS([string]$Subnet, [string]$Region){
        Set-AWSCredential -AccessKey $this.AccessKey -SecretKey $this.SecretKey
        
        ##############################################
        ## Only need this line for the volume count ##
        ##############################################
        $volumes = Get-EC2Volume -Region $this.Region | Where-Object { $_.Attachments.InstanceId -eq $this.InstanceId } 

        $RootVolume = $this.GetRootVolId()        

        # $Snapshot = $this.GetSS()
        
        $RootSnap = Get-EC2SnapShot -Region $this.Region -Owner self | Where-Object { $_.VolumeId -eq $RootVolume.VolumeId} 
        
        Write-Host "Root snap ID - " $RootSnap.SnapshotId
        $RootBlock = @{SnapshotId=$RootSnap[0].SnapshotId;DeleteOnTermination=$true}

        ###################################################################
        ## The mappings will be in order so no need to loop through them ##
        ###################################################################
        # $Mapping = $this.GetEbs()

        switch($volumes.count){     
            ###############
            ## One Drive ##
            ###############
            1{    
                $NewAMI = $this.Name + "_AMI"        ## To rename the AMI   
                            
                $ami = Register-EC2Image -Name $NewAMI -Region "us-east-1" -BlockDeviceMapping @( @{DeviceName=$this.RootDevName;Ebs=$RootBlock}) -RootDeviceName $this.RootDevName 
                $instance_params = @{
                    ImageId = $ami
                    MinCount = 1
                    MaxCount = 1
                    InstanceType = $this.InstType
                    SubnetId = $Subnet
                    Region = $Region
                    KeyName = $this.KeyName
                    AssociatePublicIp = $true
                }
                # $this.NewServer = New-EC2Instance -ImageId $ami -MinCount 1 -MaxCount 1 -InstanceType $this.InstType -SubnetId $Subnet -Region $Region -KeyName $this.KeyName -AssociatePublicIp $true
                $this.NewServer = New-EC2Instance @instance_params
                $this.SetTags()
                $this.WaitNewSS()
                Unregister-EC2Image -ImageId $ami -Region $this.Region
                break
            }
            ################
            ## Two Drives ##
            ################
            # 2{  
            #     $SecondSnap = ""    ## Placeholder for the SecondSnap variable. This needs to be instantiated here because of scoping problems
            #     $NewAMI = $this.Name + "_AMI"        ## To rename the AMI
            #     $block2Name = $Mapping[1].DeviceName

            #     foreach($snap in $Snapshot){
            #         if($snap.SnapshotId -ne $RootSnap.SnapshotId){
            #             $SecondSnap = $snap.SnapshotId  
            #         }
            #     }

            #     $NewVol = New-EC2Volume -SnapshotId $SecondSnap -AvailabilityZone "us-east-1a" -Region $this.Region

            #     $block2 = @{SnapshotId=$SecondSnap;DeleteOnTermination=$true}
            #     $ami = Register-EC2Image -Name $NewAMI  -Region "us-east-1" -BlockDeviceMapping @( @{DeviceName=$this.RootDevName;Ebs=$RootBlock}) -RootDeviceName $this.RootDevName
            #     $this.NewServer = New-EC2Instance -ImageId $ami -MinCount 1 -MaxCount 1 -KeyName $this.KeyName -InstanceType $this.InstType -Region $this.Region
            #     $this.SetTags()
            #     $this.WaitNewSS()
            #     Add-EC2Volume -InstanceId $this.NewServer.Instances.InstanceId -Device $Mapping[1].DeviceName -VolumeId $NewVol.VolumeId -Region $this.Region
            #     Unregister-EC2Image -ImageId $ami -Region $this.Region
            #     break
            # }
            # ##################
            # ## Three Drives ##
            # ##################
            # 3{  
            #     $SecondSnap = ""    ## Placeholder for the SecondSnap variable. This needs to be instantiated here because of scoping problems
            #     $ThirdSnap = ""     ## Placeholder for the ThirdSnap variable. This needs to be instantiated here because of scoping problems

            #     $block2Name = $Mapping[1].DeviceName
            #     $block3Name = $Mapping[2].DeviceName


            #     foreach($snap in $Snapshot){
            #         if($snap.SnapshotId -ne $RootSnap.SnapshotId){
            #             if($SecondSnap -eq ""){
            #                 $SecondSnap = $snap.SnapshotId 
            #             }
            #         }
            #     }
            #     $block2 = @{SnapshotId=$SecondSnap;DeleteOnTermination=$true}
            #     $ami = Register-EC2Image -Name $this.Name + "_AMI" -Region "us-east-1" -BlockDeviceMapping @( @{DeviceName=$this.RootDevName;Ebs=$RootBlock}, @{DeviceName=$block2Name;Ebs=$block2}) -RootDeviceName $this.RootDevName
            #     $this.NewServer = New-EC2Instance -ImageId $ami -MinCount 1 -MaxCount 1 -KeyName $this.KeyName -InstanceType $this.InstType -SubnetId $this.SubnetId -Region $this.Region
            #     $this.SetTags()
            #     Unregister-EC2Image -ImageId $ami -Region $this.Region
            #     break
            # }
        }
    }


}